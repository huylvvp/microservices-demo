package com.microservices.demo.twitter.to.kafka.service.exception;

public class TwitterRuntimeException extends RuntimeException {
    public TwitterRuntimeException() {
        super();
    }

    public TwitterRuntimeException(String message) {
        super(message);
    }
}
