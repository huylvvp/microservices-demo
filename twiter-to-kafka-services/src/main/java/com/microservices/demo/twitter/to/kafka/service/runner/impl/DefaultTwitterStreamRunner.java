package com.microservices.demo.twitter.to.kafka.service.runner.impl;

import com.microservices.demo.config.TwitterConfig;
import com.microservices.demo.twitter.to.kafka.service.listener.TwitterStatusListener;
import com.microservices.demo.twitter.to.kafka.service.runner.StreamRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import twitter4j.*;

import javax.annotation.PreDestroy;

@Component
@ConditionalOnProperty(name = "twitter-to-kafka-service.enable-mock-tweets", havingValue = "false", matchIfMissing = true)
public class DefaultTwitterStreamRunner implements StreamRunner {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTwitterStreamRunner.class);

    private final TwitterConfig twitterConfig;
    private final TwitterStatusListener twitterStatusListener;

    private TwitterStream twitterStream;

    public DefaultTwitterStreamRunner(TwitterConfig twitterConfig, TwitterStatusListener twitterStatusListener) {
        this.twitterConfig = twitterConfig;
        this.twitterStatusListener = twitterStatusListener;
    }

    @Override
    public void start() throws TwitterException {
        twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.addListener(twitterStatusListener);
        addFilter();
    }

    @PreDestroy
    public void shutdown() {
        if (twitterStream !=null) {
            LOG.info("shutting down");
            twitterStream.shutdown();
        }
    }

    private void addFilter() {
        String[] keyword = twitterConfig.getTwitterKeywords().toArray(new String[0]);
        FilterQuery filterQuery = new FilterQuery(keyword);
        twitterStream.filter(filterQuery);
    }
}
